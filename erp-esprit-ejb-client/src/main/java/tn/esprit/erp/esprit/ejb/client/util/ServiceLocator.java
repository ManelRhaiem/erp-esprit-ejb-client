package tn.esprit.erp.esprit.ejb.client.util;

import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ServiceLocator {

	private Context context;
	private Map<String, Object> cache;
	private static ServiceLocator instance;
	
	public static ServiceLocator getInstance() {
		if(instance==null){
			return new ServiceLocator();
		}
		return instance;
	}
	
	private ServiceLocator() {
		cache = new HashMap<>();
	}

	public Object getProxy(String jndiName) {
		if(cache.containsKey(jndiName)){
			return cache.get(jndiName);
		}
		else{
			try {
				context = new InitialContext();
				cache.put(jndiName, context.lookup(jndiName));
				return cache.get(jndiName);
			} catch (NamingException e) {
				return null;
			}

		}
		
	}

}
