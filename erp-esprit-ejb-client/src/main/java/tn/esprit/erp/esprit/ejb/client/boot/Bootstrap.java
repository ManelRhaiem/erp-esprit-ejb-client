package tn.esprit.erp.esprit.ejb.client.boot;

import java.util.Date;

import tn.esprit.erp.esprit.ejb.client.delegates.StudentCrudDelegate;
import tn.esprit.erpespritejb.entites.Student;

public class Bootstrap {

	public static void main(String[] args) {
		System.out.println("Iam in boot");

		StudentCrudDelegate.addStudent(new Student("Mohamed Amine", "BESSROUR",
				new Date(), false, false));

		System.out.println("boot is over");
	}

}
