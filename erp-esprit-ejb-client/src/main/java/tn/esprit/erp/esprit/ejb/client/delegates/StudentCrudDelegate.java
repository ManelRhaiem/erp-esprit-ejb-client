package tn.esprit.erp.esprit.ejb.client.delegates;

import java.util.List;

import tn.esprit.erp.esprit.ejb.client.util.ServiceLocator;
import tn.esprit.erpespritejb.entites.Student;
import tn.esprit.erpespritejb.services.contracts.IStudentCrudRemote;

public class StudentCrudDelegate {

	static IStudentCrudRemote studentService = (IStudentCrudRemote) ServiceLocator
			.getInstance()
			.getProxy(
					"erp-esprit-ejb/StudentCrud!tn.esprit.erpespritejb.services.contracts.IStudentCrudRemote");

	public static void addStudent(Student student) {
		studentService.addStudent(student);

	}

	public static void updateStudent(Student student) {
		studentService.updateStudent(student);
	}

	public static void deleteStudent(int id) {
		studentService.deleteStudent(id);
	}

	public static void deleteStudent(Student student) {
		studentService.deleteStudent(student);

	}

	public static Student findStudentById(int id) {
		return findStudentById(id);
	}

	public static List<Student> findAllStudent() {
		// TODO Auto-generated method stub
		return null;
	}

}
