package tn.esprit.erp.esprit.ejb.client.delegates;

import java.util.List;

import tn.esprit.erp.esprit.ejb.client.util.ServiceLocator;
import tn.esprit.erpespritejb.entites.Teacher;
import tn.esprit.erpespritejb.services.contracts.ITeacherCrudRemote;

public class TeacherCrudDelegate {

	static ITeacherCrudRemote teacherService = (ITeacherCrudRemote) ServiceLocator.getInstance().
			getProxy("erp-esprit-ejb/TeacherCrud!tn.esprit.erpespritejb.services.contracts.ITeacherCrudRemote");

	public static void addTeacher(Teacher teacher) {
		teacherService.addTeacher(teacher);

	}

	public static void updateTeacher(Teacher teacher) {
		teacherService.updateTeacher(teacher);
	}

	public static void deleteTeacher(int id) {
		teacherService.deleteTeacher(id);
	}

	public static void deleteTeacher(Teacher teacher) {
		teacherService.deleteTeacher(teacher);

	}

	public static Teacher findTeacherById(int id) {
		return findTeacherById(id);
	}

	public static List<Teacher> findAllTeacher() {
		// TODO Auto-generated method stub
		return null;
	}

}
